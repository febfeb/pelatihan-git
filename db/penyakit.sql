DROP TABLE IF EXISTS `kategori_penyakit`;
CREATE TABLE `kategori_penyakit` (
  `id` varchar(15) NOT NULL,
  `nama` text NOT NULL,
  `parent_id` varchar(15) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `kategori_penyakit` (`id`, `nama`, `parent_id`, `status`) VALUES
('icdcat1',	'Infeksi dan Penyakit Menular Tertentu [I]',	'',	1),
('icdcat1-1',	'(A00-A09) Intestinal infectious diseases',	'icdcat1',	1),
('icdcat1-10',	'(A90-A99) Arthropod-borne viral fevers and viral haemorrhagic fevers',	'icdcat1',	1),
('icdcat1-11',	'(B00-B09) Viral infections characterised by skin and mucous membrane lesions',	'icdcat1',	1)
;


CREATE TABLE `kriteria_penyakit` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `kriteria_penyakit` (`id`, `nama`) VALUES
(1,	'Tanpa Dehidrasi'),
(2,	'Ringan / Dehidrasi Ringan'),
(3,	'Sedang / Dehidrasi Sedang'),
(4,	'Berat / Dehidrasi Berat / Dirujuk');


DROP TABLE IF EXISTS `penyakit`;
CREATE TABLE `penyakit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(15) NOT NULL,
  `nama` text NOT NULL,
  `nama_indonesia` text NOT NULL,
  `kode2` varchar(15) NOT NULL,
  `jenis_id` int(11) NOT NULL DEFAULT '1',
  `kategori_id` varchar(15) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `lb1_status` int(1) NOT NULL DEFAULT '0' COMMENT '0 = tidak masuk lb1, 1 = masuk lb1',
  PRIMARY KEY (`kode`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `penyakit` (`id`, `kode`, `nama`, `nama_indonesia`, `kode2`, `jenis_id`, `kategori_id`, `status`, `lb1_status`) VALUES
(1,	'A00',	'Cholera',	'Kolera',	'A00',	1,	'icdcat1-1',	1,	1),
(2,	'A000',	'Cholera due to Vibrio cholerae 01, biovar cholerae',	'',	'A000',	1,	'icdcat1-1',	1,	0),
(3,	'A001',	'Cholera due to Vibrio cholerae 01, biovar el tor',	'',	'A00.1',	1,	'icdcat1-1',	1,	0),
(4,	'A009',	'Cholera, unspecified',	'',	'A009',	1,	'icdcat1-1',	1,	0),
(5,	'A01',	'Typhoid and paratyphoid fevers',	'Demam Typhoid dan Paratyphoid',	'A01',	1,	'icdcat1-1',	1,	1),
(6,	'A010',	'Typhoid fever',	'',	'A01.0',	1,	'icdcat1-1',	1,	0),
(7,	'A011',	'Paratyphoid fever A',	'',	'A011',	1,	'icdcat1-1',	1,	0),
(8,	'A012',	'Paratyphoid fever B',	'',	'A012',	1,	'icdcat1-1',	1,	0),
(9,	'A013',	'Paratyphoid fever C',	'',	'A013',	1,	'icdcat1-1',	1,	0),
(10,	'A014',	'Paratyphoid fever, unspecified',	'',	'A01.4',	1,	'icdcat1-1',	1,	0);


DROP TABLE IF EXISTS `penyakit_jenis`;
CREATE TABLE `penyakit_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `penyakit_jenis` (`id`, `nama`) VALUES
(1,	'Penyakit Umum'),
(2,	'Penyakit Gigi');

DROP TABLE IF EXISTS `penyakit_poli_gigi`;
CREATE TABLE `penyakit_poli_gigi` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `kode_penyakit` char(6) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `penyakit_id` char(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `penyakit_poli_gigi` (`id`, `kode_penyakit`, `nama`, `penyakit_id`) VALUES
(1,	'1501',	'Karies Gigi',	''),
(2,	'1502',	'Penyakit Pulpa & Jaringan Periapikal',	''),
(3,	'1503',	'Gingivitis & Penyakit Periodontal',	''),
(4,	'1504',	'Gangguan Gigi & Jar. Penyangga Lain/Abses Gigi',	''),
(5,	'1505',	'Peny. Rongga Mulut, Kelenjar Ludah, Radang & Lain',	'');




